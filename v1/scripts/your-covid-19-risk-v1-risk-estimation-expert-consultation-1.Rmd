---
title: "Your COVID-19 Risk: Risk Estimation Expert Consultation, round 1"
author: "Your COVID-19 Risk team"
date: "`r format(Sys.time(), '%Y-%m-%d at %H:%M:%S %Z (GMT%z)')`"
output:
  html_document:
    self_contained: yes
    always_allow_html: yes
    code_folding: hide
    toc: false
    toc_depth: 2
    keep_md: false
editor_options:
  chunk_output_type: console
---

# Introduction

```{r child = 'your-covid-19-risk-v1-intro.Rmd'}
```

```{r setup, message=FALSE}

source(here::here("v1", "scripts", "your-covid-19-risk-v1-paths.R"));

### Installing `psyverse` and `googlesheets`
# install.packages('psyverse');
# install.packages('googlesheets');
# install.packages('data.tree');
# install.packages('behaviorchange');

### If for any reason we'd want the dev version
# remotes::install_gitlab('r-packages/psyverse',
#                         dependencies=FALSE, upgrade=FALSE);

knitr::opts_chunk$set(comment="");

```

# The Risk Model

```{r load-risk-model, message=FALSE}

### Read google sheet
reDf <- 
  googlesheets::gs_read(
    googlesheets::gs_key(
      "1nqBfJhLlh18KciWGDalOA3EQ2UeQyKdExhoGXd9Sn0E",
      verbose  = FALSE
    ),
    verbose  = FALSE
  );

### Sanitize characters that are problematic for DiagrammeR
reDf$label <-
  psyverse:::sanitize_for_DiagrammeR(reDf$label);
reDf$comments <-
  psyverse:::sanitize_for_DiagrammeR(reDf$comments);

### Create brief labels
reDf$id <-
  unlist(lapply(reDf$label,
                function(x) {
                  return(strwrap(x, width = 40)[1]);
                }));

### Wrap long labels
reDf$label_for_diagrammer <-
  unlist(lapply(reDf$label,
                function(x) {
                  return(paste0(strwrap(x,
                                        width = 50),
                                collapse="\n"));
                }));

### Reorganise columns
reDf <-
  reDf[, c("id", "parent", "label", "label_for_diagrammer", "estimate", "specificity", "comments")];

### Express estimates as percentage of most
### important estimate
reDf$estimate_rescaled <-
  round(100 * reDf$estimate / max(reDf$estimate, na.rm=TRUE));

### Rescale specificity
reDf$specificity_rescaled <-
  reDf$specificity / 5;

### Set to 1 for parents
reDf$specificity_rescaled <-
  ifelse(is.na(reDf$specificity_rescaled),
         1,
         reDf$specificity_rescaled);

### Weight importance by specificity
reDf$estimate_weighed <-
  reDf$estimate_rescaled * reDf$specificity_rescaled;

### Convert to data tree
reTree <-
  data.tree::FromDataFrameNetwork(reDf);

### Compute mean estimates for parent importance
reTree$Do(function(node) {
  node$aggr_estimate <-
    round(data.tree::Aggregate(node,
                               attribute = "estimate",
                               aggFun = mean,
                               na.rm = TRUE));
  node$aggr_estimate_rescaled <-
    round(data.tree::Aggregate(node,
                               attribute = "estimate_rescaled",
                               aggFun = mean,
                               na.rm = TRUE));
  node$aggr_weighed_est <-
    round(data.tree::Aggregate(node,
                               attribute = "estimate_weighed",
                               aggFun = mean,
                               na.rm = TRUE));
  },
  traversal = "post-order");

### Back to data frame
resDf_post <-
  as.data.frame(reTree,
                row.names=NULL,
                optional = TRUE,
                'id',
                'label',
                'label_for_diagrammer',
                'estimate',
                'specificity',
                'estimate_rescaled',
                'specificity_rescaled',
                'aggr_estimate',
                'aggr_weighed_est',
                'aggr_estimate_rescaled',
                'estimate_weighed'
                );

resDf_post_parentsOnly <-
  resDf_post[is.na(resDf_post$specificity), ];

```

```{r prepare-risk-model-hierarchy-graph, fig.height=50, fig.width=12}

### These turn out to not be necessary :-/

hexadecimalColorGradient <-
  function(x, max=100, invert = FALSE) {
    if (invert) {
      return(as.hexmode(round(255 - (x / (max / 255)))));
    } else {
      return(as.hexmode(round(x / (max / 255))));
    }
  }

hexFillColor <-
  function(x) {
    increasingIntensity <- hexadecimalColorGradient(x);
    decreasingIntensity <- hexadecimalColorGradient(x, invert=TRUE);
    return(toupper(paste0("#",
                          "FF",
                          decreasingIntensity,
                          decreasingIntensity)));
  }

### Add estimates
reTree$Do(function(node) {
    data.tree::SetNodeStyle(node,
                            aggr_weighed_est = node$aggr_weighed_est,
                            estimate_rescaled = node$estimate_rescaled,
                            aggr_estimate_rescaled = node$aggr_estimate_rescaled,
                            keepExisting = TRUE);
  });

### Set some properties for plotting by DiagrammeR
data.tree::SetGraphStyle(reTree, rankdir = "LR");
data.tree::SetNodeStyle(reTree,
                        shape="box",
                        style="filled",
                        fontname = "arial",
                        keepExisting = TRUE);

```

## Risk Model: aggregated rescaled estimates, pruned 50% lowest scores

```{r risk-model-hierarchy-graph-rescaled-pruned, fig.height=17, fig.width=12}

medianEstimate_rescaled <-
  median(resDf_post$aggr_estimate_rescaled,
         na.rm=TRUE);

### Set labels as labels
reTree$Do(function(node) {
    data.tree::SetNodeStyle(node,
                            label = 
                              paste0(node$aggr_estimate_rescaled,
                                     " - ",
                                     node$label_for_diagrammer),
                            keepExisting = TRUE);
  });

prunedGraph_rescaled <-
  data.tree::ToDiagrammeRGraph(
    reTree,
    pruneFun = function(node) {
      if (node$isLeaf) {
        return(node$aggr_estimate_rescaled > medianEstimate_rescaled);
      } else {
        return(any(node$Get("aggr_estimate_rescaled") > medianEstimate_rescaled));
      }
    }
  );


prunedGraph_rescaled <-
  DiagrammeR::colorize_node_attrs(
    prunedGraph_rescaled,
    node_attr_from = "aggr_estimate_rescaled",
    node_attr_to = "fillcolor",
    palette = "viridis",
    reverse_palette = TRUE);

DiagrammeR::render_graph(prunedGraph_rescaled);

```

## Risk Model: estimates weighed by rescaled specificity and aggregated, pruned 50% lowest scores

```{r risk-model-hierarchy-graph-weighed-pruned, fig.height=17, fig.width=12}

medianEstimate_weighed <-
  median(resDf_post$aggr_weighed_est,
         na.rm=TRUE);

### Set labels as labels
reTree$Do(function(node) {
    data.tree::SetNodeStyle(node,
                            label = 
                              paste0(node$aggr_weighed_est,
                                     " - ",
                                     node$label_for_diagrammer),
                            keepExisting = TRUE);
  });

prunedGraph_weighed <-
  data.tree::ToDiagrammeRGraph(
    reTree,
    pruneFun = function(node) {
      if (node$isLeaf) {
        return(node$aggr_weighed_est > medianEstimate_weighed);
      } else {
        return(any(node$Get("aggr_weighed_est") > medianEstimate_weighed));
      }
    }
  );

prunedGraph_weighed <-
  DiagrammeR::colorize_node_attrs(
    prunedGraph_weighed,
    node_attr_from = "aggr_weighed_est",
    node_attr_to = "fillcolor",
    palette = "viridis",
    reverse_palette = TRUE);

DiagrammeR::render_graph(prunedGraph_weighed);

```

## Risk Model: aggregated rescaled estimates

```{r risk-model-hierarchy-graph-weighed, fig.height=32, fig.width=12}

### Set labels as labels
reTree$Do(function(node) {
    data.tree::SetNodeStyle(node,
                            label = 
                              paste0(node$aggr_estimate_rescaled,
                                     " - ",
                                     node$label_for_diagrammer),
                            keepExisting = TRUE);
  });

rescaledGraph <- data.tree::ToDiagrammeRGraph(reTree);

rescaledGraph <-
  DiagrammeR::colorize_node_attrs(
    rescaledGraph,
    node_attr_from = "aggr_estimate_rescaled",
    node_attr_to = "fillcolor",
    palette = "viridis",
    reverse_palette = TRUE);

DiagrammeR::render_graph(rescaledGraph);

```

## Risk Model: estimates weighed by rescaled specificity and aggregated

```{r risk-model-hierarchy-graph-rescaled, fig.height=32, fig.width=12}

### Set labels as labels
reTree$Do(function(node) {
    data.tree::SetNodeStyle(node,
                            label = 
                              paste0(node$aggr_weighed_est,
                                     " - ",
                                     node$label_for_diagrammer),
                            keepExisting = TRUE);
  });

weighedGraph <- data.tree::ToDiagrammeRGraph(reTree);

weighedGraph <-
  DiagrammeR::colorize_node_attrs(
    weighedGraph,
    node_attr_from = "aggr_weighed_est",
    node_attr_to = "fillcolor",
    palette = "viridis",
    reverse_palette = TRUE);

DiagrammeR::render_graph(weighedGraph);

```




```{r show-risk-model, results="asis"}

cat("\n\n### Overview of the hierarchy in the risk model behaviors\n\n");

reTreePrinted <-
  paste0(capture.output(print(reTree, limit=1000)),
         collapse="\n");

cat("\n\n<pre>", reTreePrinted, "</pre>\n\n");

cat("\n\n### Including estimates, specificity, and full labels\n\n");

resDf_post$levelNamePre <-
  paste0("<pre style='border:none; padding:none;'>", resDf_post$levelName, "</pre>");

### Show hierarchy
#kableExtra::kable_styling(
  knitr::kable(resDf_post[,
                          c('levelNamePre',
                            'specificity',
                            'estimate',
                            'label')],
               caption="Hierarchical order",
               format = "html",
               escape=FALSE)
#);

cat("\n\n### Sorted by (rescaled) estimate\n\n");

### Sorted by estimate_rescaled
knitr::kable(resDf_post[order(resDf_post$estimate_rescaled,
                              decreasing = TRUE),
                              c('label', 'estimate_rescaled',
                                'specificity', 'estimate')],
             caption="Sorted by (rescaled) estimate");

cat("\n\n### Sorted by weighd rescaled estimate\n\n");

### Sorted by weighed estimate_rescaled
knitr::kable(resDf_post[order(resDf_post$estimate_weighed,
                              decreasing = TRUE),
                              c('label', 'estimate_weighed',
                                'specificity', 'estimate')],
             caption="Sorted by weighd rescaled estimate");

cat("\n\n### Sorted by aggregated rescaled estimate\n\n");

### Sorted by aggr_estimate
knitr::kable(resDf_post_parentsOnly[order(resDf_post_parentsOnly$aggr_estimate,
                              decreasing = TRUE),
                              c('label', 'aggr_estimate',
                                'specificity', 'estimate')],
             caption="Sorted by aggregated rescaled estimate");

cat("\n\n\n\n");

### Sorted by aggr_weighed_est
knitr::kable(resDf_post_parentsOnly[order(resDf_post_parentsOnly$aggr_weighed_est,
                              decreasing = TRUE),
                              c('label', 'aggr_weighed_est',
                                'specificity', 'estimate')],
             caption="Sorted by aggregated weighed rescaled estimate");

```

<!----------------------------------------------------------------------------->
<!-- Enable 'hotlinks' to tabs -->
<!----------------------------------------------------------------------------->
<script>
$(document).on('ready', function () {
$('[href="'+location.hash+'"]').trigger('click');
var off = $('[href="'+location.hash+'"]').trigger('click').offset();
$('html,body').animate({scrollTop: off.top-20, scrollLeft: off.left});
$('[href^=#][data-toggle]').on('click', function() {location.hash = $(this).attr('aria-controls')});
});
</script>
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
