
source(here::here("v2", "scripts", "your-covid-19-risk-v2-paths.R"));

public_dir <- here::here("public");
v2_dir     <- file.path(public_dir, "v2");

v2_methods_dir      <- file.path(v2_dir, "methods");
v2_results_dir      <- file.path(v2_dir, "results");
v2_translations_dir <- file.path(v2_dir, "translations");

v2_dctSpecs_dir <- file.path(v2_methods_dir, "dct-specs");

if (!dir.exists(public_dir))           dir.create(public_dir);

if (!dir.exists(v2_dir))               dir.create(v2_dir);
if (!dir.exists(v2_results_dir))       dir.create(v2_results_dir);
if (!dir.exists(v2_methods_dir))       dir.create(v2_methods_dir);
if (!dir.exists(v2_translations_dir))  dir.create(v2_translations_dir);

if (!dir.exists(v2_dctSpecs_dir))      dir.create(v2_dctSpecs_dir);

v2_dctSpecs_file <-
  file.path(scriptPath,
            "your-covid-19-risk-v2-dct-specs.html");

if (file.exists(v2_dctSpecs_file)) file.copy(v2_dctSpecs_file,
                                             file.path(v2_dctSpecs_dir,
                                                       "index.html"));
