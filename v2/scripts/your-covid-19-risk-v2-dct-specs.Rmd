---
title: "Your COVID-19 Risk v2 - DCT specifications"
author: "Your COVID-19 Risk team"
date: "`r format(Sys.time(), '%Y-%m-%d at %H:%M:%S %Z (GMT%z)')`"
output:
  html_document:
    self_contained: yes
    always_allow_html: yes
    code_folding: hide
    toc: false
    toc_depth: 2
    keep_md: false
editor_options:
  chunk_output_type: console
---

<style>
div.level4 h4 {
  padding: 4px;
  border: 2px solid #000000;
  background-color: #000000;
  color: #FFFFFF;
  text-align: center;
}
div.level5 h5 {
  padding: 4px;
  border: 2px solid #000000;
  background-color: #888888;
  color: #FFFFFF;
  text-align: center;
}
</style>

# Introduction

```{r child = 'your-covid-19-risk-v2-intro.Rmd'}
```

For an explanation of what DCTs are, exactly, see https://a-bc.gitlab.io/dct-raa.

```{r setup, message=FALSE}

source(here::here("v2", "scripts", "your-covid-19-risk-v2-paths.R"));

### Installing `psyverse` and `googlesheets`
# install.packages('psyverse');
# install.packages('googlesheets');
# install.packages('data.tree');
# install.packages('behaviorchange');

### If for any reason we'd want the dev version
remotes::install_gitlab('r-packages/psyverse',
                        dependencies=FALSE, upgrade=FALSE);

knitr::opts_chunk$set(comment="");

```

# Google sheets URLs

```{r specify-googlesheets-urls}

sheetKeys <-
  c(
    "1zjRwx_vWJf3tLG59AL7m8wr8eCIAyKq9aYRJlDCdPUI"
    );

```

These DCT specifications were produced based on the following Google Sheets URLs:

`r paste(paste0("- https://docs.google.com/spreadsheets/d/", sheetKeys), collapse="\n")`

```{r read-googlesheets-urls, message=FALSE}

dctSheets <-
  lapply(
    sheetKeys,
    function(key) {
      return(
        psyverse::dct_from_gs(
          gs = key,
          path = dctPath,
          localBackup = file.path(dctSheetsPath, paste0(key, ".xlsx"))
        )
      );
    }
  );

```

# The DCT excerpts

```{r read-dcts-for-covid-19, results="asis"}

ycr_v2_dcts <-
  psyverse::load_dct_dir(
    dctPath,
    headingLevel=3,
    recursive=FALSE
  );

cat(
  unlist(
    psyverse::generate_construct_overview(ycr_v2_dcts,
                                          include=c("definition",
                                                    "measure_dev"),
                                          hideByDefault = NULL,
                                          #hideByDefault = "definition",
                                          headingLevel = 4,
                                          urlPrefix="https://a-bc.gitlab.io/dct-raa#")
    ),
  sep = "\n\n"
  );

```

<!----------------------------------------------------------------------------->
<!-- Enable 'hotlinks' to tabs -->
<!----------------------------------------------------------------------------->
<script>
$(document).on('ready', function () {
$('[href="'+location.hash+'"]').trigger('click');
var off = $('[href="'+location.hash+'"]').trigger('click').offset();
$('html,body').animate({scrollTop: off.top-20, scrollLeft: off.left});
$('[href^=#][data-toggle]').on('click', function() {location.hash = $(this).attr('aria-controls')});
});
</script>
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
